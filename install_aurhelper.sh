#!/bin/bash

mkdir /tmp/yay
cd /tmp/yay

git clone https://aur.archlinux.org/yay-git.git

cd yay-git

makepkg --syncdeps --install --noconfirm
