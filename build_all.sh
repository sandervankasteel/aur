#!/bin/bash

# exit when any command fails
set -e

for d in */ ; do
    PKG_NAME=${d%?}

    if [ "$PKG_NAME" != 'update_scripts' ]; then
        continue
    fi

    echo "Trying to build ${PKG_NAME} ..."

    # change to 'nobody'
    su -c "cd ${CI_PROJECT_DIR} && ./build_package.sh ${PKG_NAME}" - nobody -s /bin/bash

    # Clean up
    pacman -R -u --noconfirm $PKG_NAME
    
    # return to root
    cd ../
done
