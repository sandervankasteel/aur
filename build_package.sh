#!/bin/bash

PKG_NAME=$1

export BUILDDIR=/tmp/${PKG_NAME}
export PKGDEST=/tmp/pkgs

cd $PKG_NAME
source PKGBUILD

yay -V
yay -S ${depends[@]} ${makedepends[@]} --noconfirm --noeditmenu --noupgrademenu --mflags "--nocheck"
makepkg -g -f --syncdeps --install --clean --nocolor --noconfirm --noprogressbar --needed