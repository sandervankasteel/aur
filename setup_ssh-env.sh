#!/bin/bash

# exit when any command fails
set -e

tee -a /etc/ssh/ssh_config << END
Host aur aur.archlinux.org 
    User aur
    Hostname aur.archlinux.org
    IdentityFile /tmp/aur_id
    StrictHostKeyChecking no
END

chmod 600 /tmp/aur_id
eval "$(ssh-agent -s)"
ssh-add /tmp/aur_id

git config --global user.name "Automated push for Sander van Kasteel"
git config --global user.email info@sandervankasteel.nl
