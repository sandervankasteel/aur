#!/bin/bash
set -e

for d in */ ; do
    PKG_NAME=${d%?}

    aurpublish -p $PKG_NAME
    aurpublish $PKG_NAME
done